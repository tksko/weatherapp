Weather App
===========

La aplicación permite consultar información de clima actual y pronóstico de hasta 5 días de la ciudad del usuario (puede que no sea del todo exacta, ya que se utiliza un servicio externo (IP-API), basandose en información del ISP y el rango de IP pública) y adicionalmente de un listado de 5 ciudades predeterminadas.

Para el desarrollo de la misma, se utilizó una arquitectura MVP y las siguientes librerias:

* **Retrofit 2** para obtener datos  de APIS externas

* **Picasso** para la carga eficiente de imágenes remotas

* **RxJava y RxJava Android** para asincronismo y comunicación entre las distintas capas de la arqcuitectura

* **Librerias de soporte de Google** para soportar versiones anteriores de Android

* **ButterKnife** para bindeo de vistas

La información del clima es obtenida utlizando la API de **OpenWeathermap**, la cual utiliza una API KEY obtenida de forma gratuita para cada petición. Asi mismo, en cada petición se utiliza el idioma del dispositivo y se fuerza a obtener los resutlados en grados centigrados.

Adicionalmente se utiliza **IP-API** para determinar las coordneadas de la posición actual basandose en la IP pública del dispositivo.

Se incluyen como ejemplo 2 tests que prueban el caso de éxito y de error al obtener la información de clima de una posición arbitraria y como se comunican la capa de presentación con la vista utilizando **Mockito**.