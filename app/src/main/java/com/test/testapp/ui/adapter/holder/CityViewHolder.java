package com.test.testapp.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.testapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nicolas.m
 */
public class CityViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.list_item_city_content_layout)
    public LinearLayout cityLayout;

    @BindView(R.id.list_item_city_name)
    public TextView cityName;

    public CityViewHolder(View itemView) {

        super(itemView);

        ButterKnife.bind(this, itemView);

    }

}
