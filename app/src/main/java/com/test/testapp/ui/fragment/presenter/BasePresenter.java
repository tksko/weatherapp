package com.test.testapp.ui.fragment.presenter;

import android.support.v4.app.Fragment;

import com.test.testapp.business.BusinessManager;
import com.test.testapp.ui.fragment.view.BaseView;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nicolas.m
 */
public class BasePresenter<View extends BaseView> implements Presenter {

    protected View view;

    private CompositeSubscription subscriptions;

    private BusinessManager businessManager;

    public BasePresenter() {

        this.subscriptions = new CompositeSubscription();
    }

    public void setView(View view) {

        this.view = view;
    }

    public void setBusinessManager(BusinessManager businessManager) {
        this.businessManager = businessManager;
    }

    public BusinessManager getBusinessManager() {

        if (businessManager == null)
            return BusinessManager.getInstance();

        return businessManager;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

        cancelAllSubscriptions();

        this.view = null;
    }

    protected void addSubscription(Subscription subscription) {

        subscriptions.add(subscription);
    }

    private void cancelAllSubscriptions() {

        subscriptions.unsubscribe();
        subscriptions.clear();
    }

}
