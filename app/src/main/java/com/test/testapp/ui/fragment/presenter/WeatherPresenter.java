package com.test.testapp.ui.fragment.presenter;

import com.test.testapp.business.model.LocationModel;
import com.test.testapp.business.model.WeatherModel;
import com.test.testapp.ui.fragment.view.WeatherView;
import com.test.testapp.ui.model.WeatherDTO;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;

/**
 * Created by nicolas.m
 */
public class WeatherPresenter extends BasePresenter<WeatherView> {

    public void getWeather(LocationModel currentCity, boolean forceBanckend) {

        view.showWeatherLoading();

        addSubscription(getBusinessManager().getWeather(currentCity.getLat(), currentCity.getLon(), forceBanckend).subscribe(new Subscriber<WeatherModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                view.hideWeatherLoading();

                view.onError(e.getMessage());

            }

            @Override
            public void onNext(WeatherModel weatherModel) {

                view.hideWeatherLoading();

                view.onSucessGetWeather(new WeatherDTO(weatherModel));

            }

        }));

    }

    public void getForecast(LocationModel currentCity, boolean forceBanckend) {

        view.showForecastLoading();

        addSubscription(getBusinessManager().getForecast(currentCity.getLat(), currentCity.getLon(), forceBanckend).subscribe(new Subscriber<List<WeatherModel>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                view.hideForecastLoading();

                view.onError(e.getMessage());

            }

            @Override
            public void onNext(List<WeatherModel> forecast) {

                view.hideForecastLoading();

                List<WeatherDTO> dtos = new ArrayList<>();
                for (WeatherModel wm : forecast) {
                    dtos.add(new WeatherDTO(wm));
                }

                view.onSucessGetForecast(dtos);

            }
        }));

    }

}
