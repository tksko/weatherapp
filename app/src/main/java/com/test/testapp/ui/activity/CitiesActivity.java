package com.test.testapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.test.testapp.R;
import com.test.testapp.ui.fragment.CitiesFragment;

/**
 * Created by nicolas.m
 */
public class CitiesActivity extends BaseActivity {

    public final static int CITY_REQUEST_CODE = 200;

    public static Intent getCallingIntent(Context context) {

        Intent intent = new Intent(context, CitiesActivity.class);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        replaceFragment(CitiesFragment.newInstance());

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        Intent result = new Intent();
        setResult(RESULT_CANCELED, result);

        super.onBackPressed();

        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);

    }

}