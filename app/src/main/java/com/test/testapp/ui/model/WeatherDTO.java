package com.test.testapp.ui.model;

import android.text.TextUtils;

import com.test.testapp.business.model.WeatherModel;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherDTO implements Serializable {

    private String name;

    private long date;

    private String dateTxt;

    private String weatherTitle;

    private String weatherDescription;

    private String weatherIcon;

    private String temperature;

    private String temperatureMin;

    private String temperatureMax;

    public WeatherDTO(WeatherModel model) {

        this.name = model.getName();
        this.date = model.getDate();

        this.weatherTitle = model.getWeatherTitle();
        this.weatherDescription = model.getWeatherDescription();
        this.weatherIcon = model.getWeatherIcon();

        if (model.getTemperature() != null && model.getTemperature().length() > 0)
        this.temperature = model.getTemperature().substring(0, model.getTemperature().indexOf("."));

        if (model.getTemperatureMax() != null && model.getTemperatureMax().length() > 0)
        this.temperatureMax = model.getTemperatureMax().substring(0, model.getTemperatureMax().indexOf("."));

        if (model.getTemperatureMin() != null && model.getTemperatureMin().length() > 0)
        this.temperatureMin = model.getTemperatureMin().substring(0, model.getTemperatureMin().indexOf("."));

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("EE");
        try {
            Date date = formatter.parse(model.getDateTxt());
            this.dateTxt = sdf.format(date);
        } catch (ParseException e) {
            this.dateTxt = "";
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getWeatherTitle() {
        return weatherTitle;
    }

    public void setWeatherTitle(String weatherTitle) {
        this.weatherTitle = weatherTitle;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(String weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(String temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public String getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(String temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public String getDateTxt() {
        return dateTxt;
    }

    public void setDateTxt(String dateTxt) {
        this.dateTxt = dateTxt;
    }

}
