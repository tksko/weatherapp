package com.test.testapp.ui.fragment.presenter;

import com.test.testapp.business.BusinessManager;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.ui.fragment.view.LocationView;

import rx.Subscriber;

/**
 * Created by nicolas.m
 */
public class LocationPresenter extends BasePresenter<LocationView> {

    public void getCurrentLocation() {

        addSubscription(getBusinessManager().getCurrentLocation().subscribe(new Subscriber<LocationModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                view.onError(e.getMessage());

            }

            @Override
            public void onNext(LocationModel locationModel) {

                view.onSuccessGetLocation(locationModel);

            }
        }));

    }

}
