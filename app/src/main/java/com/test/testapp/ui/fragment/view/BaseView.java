package com.test.testapp.ui.fragment.view;

/**
 * Created by nicolas.m
 */
public interface BaseView {

    void onError(String message);

}
