package com.test.testapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.test.testapp.R;
import com.test.testapp.ui.adapter.holder.ForecastViewHolder;
import com.test.testapp.ui.model.WeatherDTO;
import com.test.testapp.ui.util.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastViewHolder> {

    private List<WeatherDTO> list;

    public ForecastAdapter() {

        this.list = new ArrayList();
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_forecast, parent, false);

        return new ForecastViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ForecastViewHolder holder, final int position) {

        final WeatherDTO item = list.get(position);

        if (item != null) {

            Picasso.get()
                    .load(BaseFunctions.getIconUrl(item.getWeatherIcon()))
                    .into(holder.forecastIcon);

            holder.forecastTemp.setText(item.getTemperature() + "°");

            holder.forecastDate.setText(item.getDateTxt());

        }

    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public boolean isEmpty() {

        return getItemCount() == 0;
    }

    public void updateList(List list) {

        int count = this.list.size();

        this.list = list;

        this.notifyItemRangeRemoved(0, count);

        this.notifyItemRangeInserted(0, this.list.size());
    }

    public void clear() {

        int count = this.list.size();

        this.list = new ArrayList<>();

        this.notifyItemRangeRemoved(0, count);
    }

}