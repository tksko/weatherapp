package com.test.testapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.test.testapp.R;
import com.test.testapp.config.AppConstants;
import com.test.testapp.ui.fragment.WeatherFragment;

/**
 * Created by nicolas.m
 */
public class WeatherActivity extends BaseActivity {

    public static Intent getCallingIntent(Context context, String location) {

        Intent intent = new Intent(context, WeatherActivity.class);

        intent.putExtra(AppConstants.Parameters.PARAM_CITY, location);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        replaceFragment(WeatherFragment.newInstance());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CitiesActivity.CITY_REQUEST_CODE) {

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_base_fragment_container);

            if (fragment != null && fragment instanceof WeatherFragment) {

                ((WeatherFragment) fragment).onMyActivityResult(requestCode, resultCode, data);
            }

        }

    }

}