package com.test.testapp.ui.interfaces;

/**
 * Created by nicolas.m
 */
public interface IOnItemSelectedListener {

    void onItemSelected(Object item);
}
