package com.test.testapp.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.testapp.R;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.config.AppConstants;
import com.test.testapp.ui.activity.CitiesActivity;
import com.test.testapp.ui.adapter.ForecastAdapter;
import com.test.testapp.ui.fragment.presenter.WeatherPresenter;
import com.test.testapp.ui.fragment.view.WeatherView;
import com.test.testapp.ui.model.WeatherDTO;
import com.test.testapp.ui.util.BaseFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nicolas.m
 */
public class WeatherFragment extends BaseFragment<WeatherPresenter> implements WeatherView {

    @BindView(R.id.fragment_weather_city_name)
    TextView cityName;

    @BindView(R.id.view_weather_info_temp)
    TextView currentTemp;

    @BindView(R.id.view_weather_info_icon)
    ImageView currentTempIcon;

    @BindView(R.id.view_weather_info_description)
    TextView currentTempDescription;

    @BindView(R.id.fragment_weather_info_container)
    LinearLayout currentTempContainer;

    @BindView(R.id.view_loading_view)
    View loadingView;

    @BindView(R.id.view_recycle_view_list)
    RecyclerView recyclerView;

    @BindView(R.id.view_recycle_view_empty_layout)
    View viewEmpty;

    @BindView(R.id.view_recycle_view_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private LocationModel currentCity;

    private ForecastAdapter forecastAdapter;

    public static WeatherFragment newInstance() {

        WeatherFragment fragment = new WeatherFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new WeatherPresenter();

        setHasOptionsMenu(true);

        getLocation();

    }

    private void getLocation() {

        if (getActivity().getIntent().hasExtra(AppConstants.Parameters.PARAM_CITY)) {

            String jsonString = getActivity().getIntent().getStringExtra(AppConstants.Parameters.PARAM_CITY);

            currentCity = LocationModel.fromString(jsonString);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        setupForecastView();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this);

        getWeatherData();

    }

    private void setupForecastView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        forecastAdapter = new ForecastAdapter();

        recyclerView.setAdapter(forecastAdapter);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout.setOnRefreshListener(() -> getForecastData());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.weather_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cities:
                if (getActivity() != null)
                    getActivity().startActivityForResult(CitiesActivity.getCallingIntent(getActivity()), CitiesActivity.CITY_REQUEST_CODE);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getWeatherData() {

        presenter.getWeather(currentCity, true);

    }

    private void getForecastData() {

        presenter.getForecast(currentCity, true);

    }

    @Override
    public void onError(String message) {

        setEmptyViewVisibility();

        BaseFunctions.displaySnackbar(getView(), message);

    }

    @Override
    public void showWeatherLoading() {

        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWeatherLoading() {

        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showForecastLoading() {

        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideForecastLoading() {

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSucessGetWeather(WeatherDTO weather) {

        getForecastData();

        cityName.setText(currentCity.getCity());

        currentTempDescription.setText(weather.getWeatherTitle());

        currentTemp.setText(weather.getTemperature() + "°");

        Picasso.get()
                .load(BaseFunctions.getIconUrl(weather.getWeatherIcon()))
                .into(currentTempIcon);

    }

    private void setEmptyViewVisibility() {

        viewEmpty.setVisibility(forecastAdapter.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSucessGetForecast(List<WeatherDTO> forecast) {

        forecastAdapter.updateList(forecast);

        setEmptyViewVisibility();
    }

    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CitiesActivity.CITY_REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {

                if (data.hasExtra(AppConstants.Parameters.PARAM_CITY)) {

                    LocationModel newLocation = LocationModel.fromString(data.getStringExtra(AppConstants.Parameters.PARAM_CITY));

                    if (currentCity.getId() != newLocation.getId()) {

                        currentCity = newLocation;

                        forecastAdapter.clear();

                        getWeatherData();
                    }

                }

            }

        }

    }
}
