package com.test.testapp.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.testapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nicolas.m
 */
public class ForecastViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.list_item_forecast_icon)
    public ImageView forecastIcon;

    @BindView(R.id.list_item_forecast_temp)
    public TextView forecastTemp;

    @BindView(R.id.list_item_forecast_date)
    public TextView forecastDate;

    public ForecastViewHolder(View itemView) {

        super(itemView);

        ButterKnife.bind(this, itemView);

    }

}
