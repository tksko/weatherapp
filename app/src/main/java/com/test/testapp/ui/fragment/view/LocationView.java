package com.test.testapp.ui.fragment.view;

import com.test.testapp.business.model.LocationModel;

/**
 * Created by nicolas.m
 */
public interface LocationView extends BaseView {

    void onSuccessGetLocation(LocationModel currentLocation);

}
