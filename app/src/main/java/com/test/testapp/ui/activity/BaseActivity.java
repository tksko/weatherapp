package com.test.testapp.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.test.testapp.R;
import com.test.testapp.ui.fragment.BaseFragment;

/**
 * Created by nicolas.m
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
    }

    public void replaceFragment(BaseFragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_base_fragment_container, fragment)
                .commit();

    }

}