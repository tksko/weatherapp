package com.test.testapp.ui.fragment.view;

import com.test.testapp.ui.model.WeatherDTO;

import java.util.List;

/**
 * Created by nicolas.m
 */
public interface WeatherView extends BaseView {

    void showWeatherLoading();

    void hideWeatherLoading();

    void showForecastLoading();

    void hideForecastLoading();

    void onSucessGetWeather(WeatherDTO weather);

    void onSucessGetForecast(List<WeatherDTO> forecast);
}
