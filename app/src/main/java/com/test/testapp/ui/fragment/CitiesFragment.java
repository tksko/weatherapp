package com.test.testapp.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.testapp.R;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.config.AppConstants;
import com.test.testapp.ui.adapter.CityAdapter;
import com.test.testapp.ui.fragment.presenter.CitiesPresenter;
import com.test.testapp.ui.fragment.view.CitiesView;
import com.test.testapp.ui.interfaces.IOnItemSelectedListener;
import com.test.testapp.ui.util.BaseFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nicolas.m
 */
public class CitiesFragment extends BaseFragment<CitiesPresenter> implements IOnItemSelectedListener, CitiesView {

    @BindView(R.id.view_recycle_view_list)
    RecyclerView recyclerView;

    @BindView(R.id.view_recycle_view_empty_layout)
    View viewEmpty;

    @BindView(R.id.view_recycle_view_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private CityAdapter adapter;

    public static CitiesFragment newInstance() {

        CitiesFragment fragment = new CitiesFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new CitiesPresenter();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cities, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        setRecyclerView();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this);

        getList();

    }

    private void setRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CityAdapter(this);

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout.setOnRefreshListener(() -> getList());

    }

    private void getList() {

        if (adapter == null)
            return;

        presenter.getCities();

    }

    private void setEmptyViewVisibility() {

        viewEmpty.setVisibility(adapter.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemSelected(Object city) {

        Intent result = new Intent();
        result.putExtra(AppConstants.Parameters.PARAM_CITY, BaseFunctions.toJSON(city));

        getActivity().setResult(Activity.RESULT_OK, result);

        getActivity().finish();

    }

    @Override
    public void onError(String message) {

        BaseFunctions.displaySnackbar(getView(), message);

        setEmptyViewVisibility();
    }

    @Override
    public void showCitiesLoading() {

        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideCitiesLoading() {

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSucessGetCities(List<LocationModel> cities) {

        adapter.updateList(cities);

        setEmptyViewVisibility();

    }
}
