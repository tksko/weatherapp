package com.test.testapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.testapp.R;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.ui.adapter.holder.CityViewHolder;
import com.test.testapp.ui.interfaces.IOnItemSelectedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
public class CityAdapter extends RecyclerView.Adapter<CityViewHolder> {

    private List<LocationModel> list;

    private IOnItemSelectedListener listener;

    public CityAdapter(IOnItemSelectedListener listener) {

        this.listener = listener;
        this.list = new ArrayList();
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_city, parent, false);

        return new CityViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CityViewHolder holder, final int position) {

        final LocationModel item = list.get(position);

        if (item != null) {

            holder.cityName.setText(item.getCity());

            holder.cityLayout.setOnClickListener(view -> {
                if (listener != null)
                    listener.onItemSelected(item);
            });

        }

    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public boolean isEmpty() {

        return getItemCount() == 0;
    }

    public void updateList(List list) {

        int count = this.list.size();

        this.list = list;

        this.notifyItemRangeRemoved(0, count);

        this.notifyItemRangeInserted(0, this.list.size());
    }

}