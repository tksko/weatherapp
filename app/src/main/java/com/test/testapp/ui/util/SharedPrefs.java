package com.test.testapp.ui.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.testapp.business.model.LocationModel;

import java.io.IOException;

/**
 * Created by nicolas.m
 */
public class SharedPrefs {

    private static ObjectMapper mapper = BaseFunctions.getMapper();

    private static SharedPreferences preferences = null;

    private static SharedPreferences.Editor editor = null;

    public static class Constants {

        public static final String PREFS_POSTFIX = "prefs";

        public static final String CURRENT_LOCATION = "CURRENT_LOCATION";

    }

    public static void init(Context context) {

        preferences = context.getSharedPreferences(context.getPackageName()
                + Constants.PREFS_POSTFIX, Context.MODE_PRIVATE);

        editor = preferences.edit();
    }

    public static String loadString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public static void saveString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean containsPreference(String key) {
        return preferences.contains(key);
    }

    public static void clearPreferences() {
        editor.clear();
        editor.commit();
    }

    public static String fetchString(String key) {

        String ret = "";

        if (containsPreference(key))
            ret = loadString(key, "");

        return ret;
    }

    private static void removeElement(String key) {

        if (containsPreference(key))
            preferences.edit().remove(key).commit();

    }

    public static <T> T fetchObject(Class<T> className, String key) {

        if (containsPreference(key)) {

            String stringObject = loadString(key, "");

            if (TextUtils.isEmpty(stringObject))
                return null;

            try {
                return mapper.readValue(stringObject, className);

            } catch (IOException e) {

                e.printStackTrace();

                return null;
            }

        } else {

            return null;
        }
    }

    public static void insertObjectAsJsonString(String key, Object object) {

        String json = "";

        try {
            json = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        saveString(key, json);
    }

    public static LocationModel getCurrentLocation() {

        return fetchObject(LocationModel.class, Constants.CURRENT_LOCATION);
    }

    public static void updateCurrentLocation(LocationModel newLocation) {

        insertObjectAsJsonString(Constants.CURRENT_LOCATION, newLocation);
    }
}