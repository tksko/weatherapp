package com.test.testapp.ui.fragment.view;

import com.test.testapp.business.model.LocationModel;

import java.util.List;

/**
 * Created by nicolas.m
 */
public interface CitiesView extends BaseView {

    void showCitiesLoading();

    void hideCitiesLoading();

    void onSucessGetCities(List<LocationModel> cities);
}
