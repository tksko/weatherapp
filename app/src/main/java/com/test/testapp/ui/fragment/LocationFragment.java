package com.test.testapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.testapp.R;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.ui.activity.CitiesActivity;
import com.test.testapp.ui.activity.WeatherActivity;
import com.test.testapp.ui.fragment.presenter.LocationPresenter;
import com.test.testapp.ui.fragment.view.LocationView;
import com.test.testapp.ui.util.BaseFunctions;

import butterknife.ButterKnife;

/**
 * Created by nicolas.m
 */
public class LocationFragment extends BaseFragment<LocationPresenter> implements LocationView {

    public static LocationFragment newInstance() {

        LocationFragment fragment = new LocationFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new LocationPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_location, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this);

        presenter.getCurrentLocation();
    }

    @Override
    public void onError(String message) {

        BaseFunctions.displaySnackbar(getView(), message);

        if (getActivity() != null) {

            getActivity().startActivity(CitiesActivity.getCallingIntent(getActivity()));

            getActivity().finish();
        }

    }

    @Override
    public void onSuccessGetLocation(LocationModel currentLocation) {

        if (getActivity() != null) {

            getActivity().startActivity(WeatherActivity.getCallingIntent(getActivity(), BaseFunctions.toJSON(currentLocation)));

            getActivity().finish();
        }
    }

}
