package com.test.testapp.ui.fragment.presenter;

import com.test.testapp.business.model.LocationModel;
import com.test.testapp.ui.fragment.view.CitiesView;
import com.test.testapp.ui.util.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
public class CitiesPresenter extends BasePresenter<CitiesView> {

    public void getCities() {

        view.showCitiesLoading();

        List<LocationModel> cities = new ArrayList<>();

        LocationModel savedLocation = SharedPrefs.getCurrentLocation();
        if (savedLocation != null)
            cities.add(savedLocation);

        cities.add(new LocationModel(1, "Paris", 48.861343f, 2.337339f));
        cities.add(new LocationModel(2, "Madrid", 40.416940f, -3.703788f));
        cities.add(new LocationModel(3, "Rio de Janeiro", -22.906901f, -43.181836f));
        cities.add(new LocationModel(4, "Miami", 25.760094f, -80.196170f));
        cities.add(new LocationModel(5, "Sidney", -33.872965f, 151.198784f));

        view.onSucessGetCities(cities);

        view.hideCitiesLoading();

    }

}
