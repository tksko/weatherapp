package com.test.testapp.ui.fragment.presenter;

/**
 * Created by nicolas.m
 */
public interface Presenter {

    void onResume();

    void onPause();

    void onDestroy();

}