package com.test.testapp.ui.activity;

import android.os.Bundle;

import com.test.testapp.ui.fragment.LocationFragment;

/**
 * Created by nicolas.m
 */
public class LocationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        replaceFragment(LocationFragment.newInstance());

    }

}