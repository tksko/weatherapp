package com.test.testapp.business;

import com.test.testapp.business.data.backend.BackendManager;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.business.model.WeatherModel;
import com.test.testapp.business.resolver.GetCurrentLocationBusinessResolver;
import com.test.testapp.business.resolver.GetForecastBusinessResolver;
import com.test.testapp.business.resolver.GetWeatherBusinessResolver;
import com.test.testapp.config.AppConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by nicolas.m
 */
public class BusinessManager {

    private static BusinessManager INSTANCE;

    private BackendManager backendManager;

    public static BusinessManager getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new BusinessManager();
        }

        return INSTANCE;

    }

    public BusinessManager() {

        this.backendManager = new BackendManager();
    }

    public Observable<WeatherModel> getWeather(float cityLatitude, float cityLongitude, boolean forceBanckend) {

        GetWeatherBusinessResolver coreResolver = new GetWeatherBusinessResolver(backendManager);
        coreResolver.setForceBackend(forceBanckend);

        Map<String, Object> params = new HashMap<>();
        params.put(AppConstants.Parameters.CITY_LATITUDE, cityLatitude);
        params.put(AppConstants.Parameters.CITY_LONGITUDE, cityLongitude);

        return coreResolver.getObservable(params);

    }

    public Observable<List<WeatherModel>> getForecast(float cityLatitude, float cityLongitude, boolean forceBanckend) {

        GetForecastBusinessResolver coreResolver = new GetForecastBusinessResolver(backendManager);
        coreResolver.setForceBackend(forceBanckend);

        Map<String, Object> params = new HashMap<>();
        params.put(AppConstants.Parameters.CITY_LATITUDE, cityLatitude);
        params.put(AppConstants.Parameters.CITY_LONGITUDE, cityLongitude);

        return coreResolver.getObservable(params);

    }

    public Observable<LocationModel> getCurrentLocation() {

        GetCurrentLocationBusinessResolver coreResolver = new GetCurrentLocationBusinessResolver(backendManager);

        return coreResolver.getObservable(null);

    }

}
