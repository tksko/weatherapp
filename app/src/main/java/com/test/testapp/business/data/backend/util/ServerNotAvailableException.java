package com.test.testapp.business.data.backend.util;

/**
 * Created by nicolas.m
 */
public class ServerNotAvailableException extends Exception {

    public ServerNotAvailableException() {

        super("Server not Available. Please Retry later");

    }

}
