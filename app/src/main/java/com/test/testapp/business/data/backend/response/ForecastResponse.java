package com.test.testapp.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ForecastResponse extends BaseResponse {

    @JsonProperty("list")
    private List<WeatherResponse> forecast;

    public ForecastResponse() {
        this.forecast = new ArrayList<>();
    }

    public List<WeatherResponse> getForecast() {
        return forecast;
    }

    public void setForecast(List<WeatherResponse> forecast) {
        this.forecast = forecast;
    }

}
