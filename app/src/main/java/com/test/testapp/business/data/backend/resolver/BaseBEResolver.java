package com.test.testapp.business.data.backend.resolver;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.core.type.TypeReference;
import com.test.testapp.App;
import com.test.testapp.business.data.backend.ApiService;
import com.test.testapp.business.data.backend.response.BaseErrorResponse;
import com.test.testapp.business.data.backend.util.BackendException;
import com.test.testapp.business.data.backend.util.CheckConnection;
import com.test.testapp.business.data.backend.util.NoConnectionException;
import com.test.testapp.business.data.backend.util.RestClient;
import com.test.testapp.business.data.backend.util.ServerNotAvailableException;
import com.test.testapp.ui.util.BaseFunctions;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nicolas.m
 */
public abstract class BaseBEResolver<T> implements Callback<T> {

    protected ApiService client = null;

    protected Subscriber<? super T> subscriber;

    public BaseBEResolver() {

        this.client = RestClient.INSTANCE.getApiService();
    }

    public Observable<T> makeCall(@Nullable final Map<String, Object> params) {

        return Observable.create((Observable.OnSubscribe<T>) subscriber -> {

                    BaseBEResolver.this.subscriber = subscriber;

                    if (isNetworkConnectivityAvailable()) {

                        getDataFromBackend(params);
                    } else {

                        subscriber.onError(new NoConnectionException());
                    }

                }
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected abstract void getDataFromBackend(Map<String, Object> params);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        T baseResponse;

        try {

            if (response.isSuccessful()) {

                baseResponse = response.body();

                this.onSuccessResponse(baseResponse);

                subscriber.onNext(baseResponse);

                subscriber.onCompleted();

            } else {

                String jsonResponse = response.errorBody().string();

                BaseErrorResponse errorResponse = BaseFunctions.getMapper().readValue(jsonResponse, new TypeReference<BaseErrorResponse>() {
                });

                this.onFailResponse(errorResponse);

                subscriber.onError(new BackendException(errorResponse));

            }

        } catch (Exception e) {

            int code = response.code();

            if (code >= 500) {

                e = new ServerNotAvailableException();
            }

            this.onFailResponse(e);

            subscriber.onError(e);

        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {

        this.onFailResponse(t);

        subscriber.onError(t);
    }

    protected void onSuccessResponse(T baseResponse) {
    }

    protected void onFailResponse(BaseErrorResponse baseResponse) {
    }

    protected void onFailResponse(Throwable throwable) {
    }

    private boolean isNetworkConnectivityAvailable() {

        return CheckConnection.getInstance(App.getContext()).isOnline();
    }

}
