package com.test.testapp.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.test.testapp.business.data.backend.model.TemperatureEntity;
import com.test.testapp.business.data.backend.model.WeatherEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class WeatherResponse extends BaseResponse {

    @JsonProperty("name")
    private String name;

    @JsonProperty("weather")
    private List<WeatherEntity> weatherInfo;

    @JsonProperty("main")
    private TemperatureEntity temperatureInfo;

    @JsonProperty("dt")
    private long date;

    @JsonProperty("dt_txt")
    private String dateTxt;

    public WeatherResponse() {
        this.weatherInfo = new ArrayList<>();
        this.temperatureInfo = new TemperatureEntity();
        this.name = "";
        this.date = 0;
        this.dateTxt = "";
    }

    public List<WeatherEntity> getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(List<WeatherEntity> weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public TemperatureEntity getTemperatureInfo() {
        return temperatureInfo;
    }

    public void setTemperatureInfo(TemperatureEntity temperatureInfo) {
        this.temperatureInfo = temperatureInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDateTxt() {
        return dateTxt;
    }

    public void setDateTxt(String dateTxt) {
        this.dateTxt = dateTxt;
    }
}
