package com.test.testapp.business.model;

import com.test.testapp.business.data.backend.response.BaseResponse;
import com.test.testapp.business.data.backend.response.LocationResponse;
import com.test.testapp.ui.util.BaseFunctions;

import java.io.IOException;

/**
 * Created by nicolas.m
 */
public class LocationModel extends BaseResponse {

    private long id;

    private String city;

    private float lat;

    private float lon;

    public LocationModel() {
        this.id = 0;
        this.city = "";
        this.lat = 0f;
        this.lon = 0f;
    }

    public LocationModel(long id, String city, float lat, float lon) {
        this.id = id;
        this.city = city;
        this.lat = lat;
        this.lon = lon;
    }

    public LocationModel(long id, LocationResponse entity) {
        this.id = id;
        this.city = entity.getCity();
        this.lat = entity.getLat();
        this.lon = entity.getLon();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public static LocationModel fromString(String json) {

        try {

            return BaseFunctions.getMapper().readValue(json, LocationModel.class);

        } catch (IOException e) {
            return new LocationModel();
        }

    }

    public long getId() {
        return id;
    }

}
