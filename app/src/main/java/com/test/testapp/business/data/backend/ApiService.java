package com.test.testapp.business.data.backend;

import com.test.testapp.business.data.backend.response.ForecastResponse;
import com.test.testapp.business.data.backend.response.LocationResponse;
import com.test.testapp.business.data.backend.response.WeatherResponse;
import com.test.testapp.config.AppConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by nicolas.m
 */
public interface ApiService {

    @GET(AppConstants.Endpoints.WEATHER)
    Call<WeatherResponse> getWeather(
            @Query("lat") String latitude,
            @Query("lon") String longitude
    );

    @GET(AppConstants.Endpoints.FORECAST)
    Call<ForecastResponse> getForecast(
            @Query("lat") String latitude,
            @Query("lon") String longitude
    );

    @GET
    Call<LocationResponse> getCurrentLocation(
            @Url String url
    );

}
