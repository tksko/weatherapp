package com.test.testapp.business.resolver;

import android.support.annotation.Nullable;

import com.test.testapp.App;
import com.test.testapp.business.data.backend.BackendManager;
import com.test.testapp.business.data.backend.util.CheckConnection;
import com.test.testapp.business.data.backend.util.NoConnectionException;

import java.util.Map;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by nicolas.m
 */
public class BaseBusinessResolver<T, Z> extends Subscriber<Z> {

    protected BackendManager backendManager;

    protected boolean forceBackend;

    protected Subscriber<? super T> subscriber;

    protected Map<String, Object> params;

    public BaseBusinessResolver(BackendManager backendManager) {

        this.backendManager = backendManager;

        this.forceBackend = false;
    }

    public Observable<T> getObservable(@Nullable final Map<String, Object> params) {

        return Observable.create(subscriber -> {

                    BaseBusinessResolver.this.subscriber = subscriber;

                    BaseBusinessResolver.this.params = params;

                    getObservableActionsFlow();

                }
        );
    }

    protected void getObservableActionsFlow() {

        if (!isNetworkConnectivityAvailable()) {

            subscriber.onError(new NoConnectionException());

        } else if (backendManager != null) {

            callBackend();
        }

    }

    protected void callBackend() {

        subscriber.onCompleted();
    }

    protected void onSuccessCallActions(Z response) {

        subscriber.onCompleted();
    }

    protected void onFailureCallActions(Throwable e) {

        checkForBackendException(e);

        subscriber.onError(e);
    }

    protected void callDatabase() {

        subscriber.onCompleted();
    }

    public void setForceBackend(boolean forceBackend) {

        this.forceBackend = forceBackend;
    }

    protected boolean isNetworkConnectivityAvailable() {

        return CheckConnection.getInstance(App.getContext()).isOnline();
    }

    protected void checkForBackendException(Throwable e) {

    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

        onFailureCallActions(e);

    }

    @Override
    public void onNext(Z response) {

        onSuccessCallActions(response);

    }

}
