package com.test.testapp.business.data.backend.resolver;

import com.test.testapp.business.data.backend.response.WeatherResponse;
import com.test.testapp.config.AppConstants;

import java.util.Map;

/**
 * Created by nicolas.m
 */
public class GetWeatherBEResolver extends BaseBEResolver<WeatherResponse> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        client.getWeather(
                String.valueOf(params.get(AppConstants.Parameters.CITY_LATITUDE)),
                String.valueOf(params.get(AppConstants.Parameters.CITY_LONGITUDE))
        ).enqueue(this);

    }

}
