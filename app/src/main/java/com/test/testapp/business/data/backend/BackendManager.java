package com.test.testapp.business.data.backend;

import com.test.testapp.business.data.backend.resolver.GetCurrentLocationBEResolver;
import com.test.testapp.business.data.backend.resolver.GetForecastBEResolver;
import com.test.testapp.business.data.backend.resolver.GetWeatherBEResolver;
import com.test.testapp.business.data.backend.response.ForecastResponse;
import com.test.testapp.business.data.backend.response.LocationResponse;
import com.test.testapp.business.data.backend.response.WeatherResponse;

import java.util.Map;

import rx.Observable;

/**
 * Created by nicolas.m
 */
public class BackendManager {

    public BackendManager() {

    }

    public Observable<WeatherResponse> getWeather(Map<String, Object> params) {

        GetWeatherBEResolver backendResolver = new GetWeatherBEResolver();

        return backendResolver.makeCall(params);

    }

    public Observable<ForecastResponse> getForecast(Map<String, Object> params) {

        GetForecastBEResolver backendResolver = new GetForecastBEResolver();

        return backendResolver.makeCall(params);

    }

    public Observable<LocationResponse> getCurrentLocation() {

        GetCurrentLocationBEResolver backendResolver = new GetCurrentLocationBEResolver();

        return backendResolver.makeCall(null);

    }

}
