package com.test.testapp.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.test.testapp.business.data.backend.model.TemperatureEntity;
import com.test.testapp.business.data.backend.model.WeatherEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class LocationResponse extends BaseResponse {

    @JsonProperty("city")
    private String city;

    @JsonProperty("lat")
    private float lat;

    @JsonProperty("lon")
    private float lon;

    public LocationResponse() {
        this.city = "";
        this.lat = 0f;
        this.lon = 0f;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
