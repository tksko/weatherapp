package com.test.testapp.business.data.backend.util;

/**
 * Created by nicolas.m
 */
public class NoConnectionException extends Exception {

    public NoConnectionException() {

        super("No network connectivity available");

    }

}
