package com.test.testapp.business.model;

import com.test.testapp.business.data.backend.model.WeatherEntity;
import com.test.testapp.business.data.backend.response.BaseResponse;
import com.test.testapp.business.data.backend.response.WeatherResponse;

/**
 * Created by nicolas.m
 */
public class WeatherModel extends BaseResponse {

    private String name;

    private long date;

    private String dateTxt;

    private String weatherTitle;

    private String weatherDescription;

    private String weatherIcon;

    private String temperature;

    private String temperatureMin;

    private String temperatureMax;

    public WeatherModel() {
        this.name = "";
        this.date = 0;
        this.dateTxt = "";
        this.weatherIcon = "";
        this.weatherDescription = "";
        this.weatherTitle = "";
        this.temperatureMin = "";
        this.temperatureMax = "";
        this.temperature = "";
    }

    public WeatherModel(WeatherResponse entity) {

        this.name = entity.getName();
        this.date = entity.getDate();
        if (entity.getWeatherInfo().size() > 0) {
            WeatherEntity weatherInfo = entity.getWeatherInfo().get(0);
            this.weatherTitle = weatherInfo.getMain();
            this.weatherDescription = weatherInfo.getDescription();
            this.weatherIcon = weatherInfo.getIcon();
        } else {
            this.weatherTitle = "";
            this.weatherDescription = "";
            this.weatherIcon = "";
        }

        this.temperature = String.valueOf(entity.getTemperatureInfo().getTemp());
        this.temperatureMax = String.valueOf(entity.getTemperatureInfo().getTempMax());
        this.temperatureMin = String.valueOf(entity.getTemperatureInfo().getTempMin());

        this.dateTxt = entity.getDateTxt();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getWeatherTitle() {
        return weatherTitle;
    }

    public void setWeatherTitle(String weatherTitle) {
        this.weatherTitle = weatherTitle;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(String weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(String temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public String getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(String temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public String getDateTxt() {
        return dateTxt;
    }

    public void setDateTxt(String dateTxt) {
        this.dateTxt = dateTxt;
    }
}
