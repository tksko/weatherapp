package com.test.testapp.business.resolver;

import com.test.testapp.business.data.backend.BackendManager;
import com.test.testapp.business.data.backend.response.WeatherResponse;
import com.test.testapp.business.model.WeatherModel;

/**
 * Created by nicolas.m
 */
public class GetWeatherBusinessResolver extends BaseBusinessResolver<WeatherModel, WeatherResponse> {

    public GetWeatherBusinessResolver(BackendManager backendManager) {
        super(backendManager);
    }

    @Override
    protected void callBackend() {

        backendManager.getWeather(params).subscribe(this);

    }

    @Override
    protected void onSuccessCallActions(WeatherResponse response) {

        subscriber.onNext(new WeatherModel(response));

        subscriber.onCompleted();

    }

}
