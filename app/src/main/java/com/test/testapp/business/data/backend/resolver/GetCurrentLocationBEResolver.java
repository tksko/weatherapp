package com.test.testapp.business.data.backend.resolver;

import com.test.testapp.business.data.backend.response.LocationResponse;
import com.test.testapp.config.AppConstants;

import java.util.Map;

/**
 * Created by nicolas.m
 */
public class GetCurrentLocationBEResolver extends BaseBEResolver<LocationResponse> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        client.getCurrentLocation(
                AppConstants.CURRENT_LOCATION_URL
        ).enqueue(this);

    }

}
