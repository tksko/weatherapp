package com.test.testapp.business.resolver;

import com.test.testapp.business.data.backend.BackendManager;
import com.test.testapp.business.data.backend.response.LocationResponse;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.ui.util.SharedPrefs;

/**
 * Created by nicolas.m
 */
public class GetCurrentLocationBusinessResolver extends BaseBusinessResolver<LocationModel, LocationResponse> {

    public GetCurrentLocationBusinessResolver(BackendManager backendManager) {
        super(backendManager);
    }

    @Override
    protected void callBackend() {

        backendManager.getCurrentLocation().subscribe(this);

    }

    @Override
    protected void onFailureCallActions(Throwable e) {

        LocationModel currentLocation = SharedPrefs.getCurrentLocation();

        if (currentLocation != null) {

            subscriber.onNext(currentLocation);

            subscriber.onCompleted();

        } else {

            super.onFailureCallActions(e);

        }

    }

    @Override
    protected void onSuccessCallActions(LocationResponse response) {

        LocationModel newLocation = new LocationModel(0, response);

        SharedPrefs.updateCurrentLocation(newLocation);

        subscriber.onNext(newLocation);

        subscriber.onCompleted();

    }

}
