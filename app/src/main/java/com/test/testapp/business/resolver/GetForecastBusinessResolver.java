package com.test.testapp.business.resolver;

import com.test.testapp.business.data.backend.BackendManager;
import com.test.testapp.business.data.backend.response.ForecastResponse;
import com.test.testapp.business.data.backend.response.WeatherResponse;
import com.test.testapp.business.model.WeatherModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m
 */
public class GetForecastBusinessResolver extends BaseBusinessResolver<List<WeatherModel>, ForecastResponse> {

    public GetForecastBusinessResolver(BackendManager backendManager) {
        super(backendManager);
    }

    @Override
    protected void callBackend() {

        backendManager.getForecast(params).subscribe(this);

    }

    @Override
    protected void onSuccessCallActions(ForecastResponse response) {

        List<WeatherModel> forecast = new ArrayList<>();

        for (WeatherResponse entity : response.getForecast()) {
            if (entity.getDateTxt().contains("00:00:00"))
                forecast.add(new WeatherModel(entity));
        }

        subscriber.onNext(forecast);

        subscriber.onCompleted();

    }

}
