package com.test.testapp.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by nicolas.m
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class BaseErrorResponse {

    public BaseErrorResponse() {
    }
    
}
