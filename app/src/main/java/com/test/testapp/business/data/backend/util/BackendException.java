package com.test.testapp.business.data.backend.util;

import com.test.testapp.business.data.backend.response.BaseErrorResponse;

/**
 * Created by nicolas.m
 */
public class BackendException extends Exception {

    public BackendException() {
        super();
    }

    public BackendException(BaseErrorResponse errorResponse) {
        super();
    }

}
