package com.test.testapp.business.data.backend.util;

import com.test.testapp.BuildConfig;
import com.test.testapp.business.data.backend.ApiService;
import com.test.testapp.config.AppConstants;
import com.test.testapp.ui.util.BaseFunctions;

import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by nicolas.m
 */
public enum RestClient {

    INSTANCE;

    private ApiService apiService;

    private OkHttpClient okHttpClient = new OkHttpClient();

    public ApiService getApiService() {

        if (apiService == null) {

            synchronized (INSTANCE) {
                if (apiService == null)
                    init();
            }
        }

        return apiService;

    }

    private void init() {

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(configureHttpClient())
                .addConverterFactory(JacksonConverterFactory.create(BaseFunctions.getMapper()))
                .build();

        apiService = restAdapter.create(ApiService.class);

    }

    private OkHttpClient configureHttpClient() {

        configureTimeouts();

        return okHttpClient;

    }

    private void configureTimeouts() {

        okHttpClient = okHttpClient.newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(chain -> {

                    Request original = chain.request();

                    HttpUrl originalHttpUrl = original.url();

                    HttpUrl url = originalHttpUrl.newBuilder()
                            .addQueryParameter("APPID", AppConstants.WEATHER_API_KEY)
                            .addQueryParameter("lang", BaseFunctions.getApplicationLanguage())
                            .addQueryParameter("units", AppConstants.Metrics.CELSIUS)
                            .build();

                    Request.Builder requestBuilder = original.newBuilder().url(url);

                    Request newRequest = requestBuilder.build();

                    return chain.proceed(newRequest);

                })
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

    }

}
