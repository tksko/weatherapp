package com.test.testapp;

import android.app.Application;
import android.content.Context;

import com.test.testapp.ui.util.SharedPrefs;

/**
 * Created by nicolas.m
 */
public class App extends Application {

    private static Context context;

    @Override
    public void onCreate() {

        super.onCreate();

        context = this;

        SharedPrefs.init(this);

    }

    public static Context getContext() {
        return context;
    }

}
