package com.test.testapp.config;

/**
 * Created by nicolas.m
 */
public class AppConstants {

    public static final String APP_TAG = "TestApp";

    public static final String WEATHER_API_KEY = "43b57b2b0d83d9eb2b153147e7324042";

    public static final String WEATHER_ICONS_URL = "http://openweathermap.org/img/w/";

    public static final String CURRENT_LOCATION_URL = "http://ip-api.com/json/";

    public static class Endpoints {

        public static final String WEATHER = "weather";

        public static final String FORECAST = "forecast";

    }

    public static class Metrics {

        public static final String FAHRENHEIT = "imperial";

        public static final String CELSIUS = "metric";
    }

    public static class Parameters {

        public static final String CITY_LATITUDE = "city_latitude";

        public static final String CITY_LONGITUDE = "city_longitude";

        public static final String PARAM_CITY = "param_city";

    }

}
