package com.test.testapp;

import com.test.testapp.business.BusinessManager;
import com.test.testapp.business.model.LocationModel;
import com.test.testapp.business.model.WeatherModel;
import com.test.testapp.ui.fragment.WeatherFragment;
import com.test.testapp.ui.fragment.presenter.WeatherPresenter;
import com.test.testapp.ui.fragment.view.WeatherView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by nicolas.m
 */

public class WeatherPresenterTest {

    @Mock
    private BusinessManager businessManager;

    @Mock
    private WeatherView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchValidDataShouldLoadIntoView() {

        WeatherModel weatherModel = new WeatherModel();

        when(businessManager.getWeather(0f, 0f, true))
                .thenReturn(Observable.just(weatherModel));

        WeatherPresenter presenter = new WeatherPresenter();
        presenter.setBusinessManager(businessManager);
        presenter.setView(view);

        presenter.getWeather(new LocationModel(), true);

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view, times(1)).showWeatherLoading();
        inOrder.verify(view, times(1)).hideWeatherLoading();
        inOrder.verify(view, times(1)).onSucessGetWeather(any());
    }

    @Test
    public void fetchErrorShouldReturnErrorToView() {

        String message = "Testing Exception";
        Exception exception = new Exception(message);

        when(businessManager.getWeather(0f, 0f, true))
                .thenReturn(Observable.error(exception));

        WeatherPresenter presenter = new WeatherPresenter();
        presenter.setBusinessManager(businessManager);
        presenter.setView(view);

        presenter.getWeather(new LocationModel(), true);

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view, times(1)).showWeatherLoading();
        inOrder.verify(view, times(1)).hideWeatherLoading();
        inOrder.verify(view, times(1)).onError(message);
        verify(view, never()).onSucessGetWeather(any());
    }

}
